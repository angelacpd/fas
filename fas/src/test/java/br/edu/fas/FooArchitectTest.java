package br.edu.fas;

import org.junit.Test;
import br.edu.fas.persistence.Dao;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.lang.ArchRule;

import static com.tngtech.archunit.core.importer.ImportOption.DoNotIncludeTests;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;
import static com.tngtech.archunit.library.dependencies.SlicesRuleDefinition.slices;
import static com.tngtech.archunit.library.Architectures.layeredArchitecture;

public class FooArchitectTest {
  // JavaClasses importedClasses = new ClassFileImporter().importPackages("br.edu.fas");

  ImportOption ignoreTests = new DoNotIncludeTests();

  JavaClasses importedClasses = new ClassFileImporter().withImportOption(ignoreTests).importPackages("br.edu.fas");

  @Test
  public void verificarDependeciaParaCamadaPersistence() {
    
    ArchRule rule = classes()
    .that().resideInAPackage("..persistence..")
    .should().onlyHaveDependentClassesThat().resideInAnyPackage("..persistence..", "..service..");

    rule.check(importedClasses);
  }

  @Test
  public void verificarDependeciaDaCamadaPersistence() {
    
    ArchRule rule = noClasses()
    .that().resideInAPackage("..persistence..")
    .should().dependOnClassesThat().resideInAnyPackage("..service..");

    rule.check(importedClasses);
  }

  @Test
  public void verificarNomesCamadaPersistence() {
    
    ArchRule rule = classes()
    .that().haveSimpleNameEndingWith("Dao")
    .should().resideInAnyPackage("..persistence..");

    rule.check(importedClasses);
  }


  @Test
  public void verificarImplementacaoInterfaceDao() {
    
    ArchRule rule = classes()
    .that().implement(Dao.class)
    .should().haveSimpleNameEndingWith("Dao");

    rule.check(importedClasses);
  }

  @Test
  public void verificarDependenciasCiclicas() {
    
    ArchRule rule = slices()
    .matching("br.edu.fas.(*)..").should().beFreeOfCycles();

    rule.check(importedClasses);
  }

  @Test
  public void verificarViolacaoCamadas() {
    
    ArchRule rule = layeredArchitecture()
    .layer("Service").definedBy("..service..")
    .layer("Persistence").definedBy("..persistence..")
    .whereLayer("Persistence").mayOnlyBeAccessedByLayers("Service");

    rule.check(importedClasses);
  }


}
